package com.base;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.util.BrowserFactory;
import com.util.Helper;
import com.util.ReadPropertiesFile;

public class Base {
	public static boolean enableLogs;
	public static WebDriver driver;
	public static ReadPropertiesFile prop;
	public BrowserFactory init;
	public static ExtentReports report;
	public ExtentTest logger;
	
	@BeforeSuite
	public void SetupSuite(){
		prop = new ReadPropertiesFile();
//		enableLogs = prop.GetDataFromConfig("enablelogs").equals("true");
//		ExtentHtmlReporter extent = new ExtentHtmlReporter("./reports/verification_"+Helper.DateFormater()+".html");
//		report = new ExtentReports();
//		report.attachReporter(extent);
	}
	
	@BeforeMethod
	public void SetupClass(){
		init = new BrowserFactory();
		driver = init.InitializeDriver(prop.GetDataFromConfig("browser"));
	}
	
	@AfterMethod
	public void TearDown(ITestResult result) throws IOException{
//		String path;
//		Helper helper = new Helper();
//		if(result.getStatus() == ITestResult.SUCCESS){
//			path = helper.takeScreenshot();
//			logger.pass("Test passed", MediaEntityBuilder.createScreenCaptureFromPath(path).build());
//		}else if(result.getStatus() == ITestResult.FAILURE){
//			path = helper.takeScreenshot();
//			logger.fail("Test failed", MediaEntityBuilder.createScreenCaptureFromPath(path).build());
//		}
//		driver.quit();
	}
	
//	@AfterSuite
//	public void tearDown(){
//		report.flush();
//	}
}
