package com.pages;

import java.util.List;

import javax.swing.plaf.basic.BasicSliderUI.ActionScroller;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.base.Base;

public class PracticePage extends Base{
	
	@FindBy(id="bmwradio")
	WebElement radioBtnBmw;
	
	@FindBy(id="benzradio")
	WebElement radioBtnBenz;
	
	@FindBy(id="hondaradio")
	WebElement radioBtnHonda;
	
	@FindBy(id="carselect")
	WebElement selectDropDown;
	
	@FindBy(id="multiple-select-example")
	WebElement SelectMultiple;
	
	@FindBy(id="bmwcheck")
	WebElement checkBmw;
	
	@FindBy(id="benzcheck")
	WebElement checkBenz;
	
	@FindBy(id="hondacheck")
	WebElement checkHonda;
	
	@FindBy(id="openwindow")
	WebElement openNewWindow;
	
	@FindBy(id="opentab")
	WebElement openNewTab;
	
	@FindBy(id="alertbtn")
	WebElement alertAlert;
	
	@FindBy(id="confirmbtn")
	WebElement alertConfirm;
	
	@FindBy(id="product")
	WebElement table;
	
	@FindBy(id="hide-textbox")
	WebElement textboxHide;
		
	@FindBy(id="show-textbox")
	WebElement textboxShow;
	
	@FindBy(id="displayed-text")
	WebElement textbox;
	
	@FindBy(id="mousehover")
	WebElement mouseOver;
	
	@FindBy(linkText="Top")
	WebElement linkTextTop;
	
	public PracticePage(){
		PageFactory.initElements(driver, this);
	}
	
	public void selectRadio(String car){
		if(car.equalsIgnoreCase("bmw"))
			radioBtnBmw.click();
		else if(car.equalsIgnoreCase("benz"))
			radioBtnBenz.click();
		else if(car.equalsIgnoreCase("honda"))
			radioBtnHonda.click();
	}
	
	public void selectSelect(String car){
		new Select(selectDropDown).selectByValue(car);
	}
	
	public void selectMultiple(String ar1, String ar2){
		Select sel = new Select(SelectMultiple);
		sel.selectByVisibleText(ar1);
		sel.selectByVisibleText(ar2);
	}
	
	public void selectCheckbox(String car){
		if(car.equalsIgnoreCase("bmw"))
			checkBmw.click();
		else if(car.equalsIgnoreCase("benz"))
			checkBenz.click();
		else if(car.equalsIgnoreCase("honda"))
			checkHonda.click();
	}
	
	public void OpenNewWindow(){
		String mainHandle = driver.getWindowHandle();
		System.out.println(mainHandle);
		openNewWindow.click();
		for(String handle: driver.getWindowHandles()){
			System.out.println(handle);
			driver.switchTo().window(handle);
			if(!driver.getWindowHandle().equals(mainHandle)){
				System.out.println("condition passed");
				driver.close();
				driver.switchTo().window(mainHandle);
			}
		}
	}
	
	public void OpenNewTab(){
		String mainHandle = driver.getWindowHandle();
		System.out.println(mainHandle);
		openNewTab.click();
		for(String handle: driver.getWindowHandles()){
			System.out.println(handle);
			driver.switchTo().window(handle);
			if(!driver.getWindowHandle().equals(mainHandle)){
				System.out.println("condition passed");
				driver.close();
				driver.switchTo().window(mainHandle);
			}
		}
	}
	
	public void alertAccept(){
		alertAlert.click();
		driver.switchTo().alert().accept();
	}
	
	public void alertDismiss(){
		alertConfirm.click();
		driver.switchTo().alert().dismiss();
	}
	
	public void printWebTableContent(){	
		List<WebElement> row,col;
		row = table.findElements(By.tagName("tr"));
		for(WebElement r:row){
			col = r.findElements(By.tagName("th"));
			for(WebElement c:col)
				System.out.print(c.getText()+ " | ");
			col = r.findElements(By.tagName("td"));
			for(WebElement c:col)
				System.out.print(c.getText()+ " | ");
			System.out.println();
		}
	}
	
	public void hideTextBox(){
		textboxHide.click();
	}
	
	public void showTextBox(){
		textboxShow.click();
	}
	
	public boolean isTextBoxVisible(){
		return textbox.isDisplayed();
	}
	
	public void moveMouseOver(){
		Actions action = new Actions(driver);
		action.moveToElement(mouseOver).build().perform();
		action.moveToElement(linkTextTop).click().build().perform();
	}
	
	public void pageActions(){
//		selectRadio(car);
	}
}
