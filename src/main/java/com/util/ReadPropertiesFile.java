package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import com.base.Base;

public class ReadPropertiesFile extends Base {
	Properties prop;
	public ReadPropertiesFile(){
		prop = new Properties();
		File config = new File("./src/main/java/com/config/config.properties");
		try {
			FileInputStream fis = new FileInputStream(config);
			prop.load(fis);
		} catch (Exception e) {
			System.out.println("Invalid data file 1");
		}
	}
	
	public String GetDataFromConfig(String key){
		return prop.getProperty(key);
	}
}
